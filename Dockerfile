FROM node:12.6

RUN mkdir -p /src/app
WORKDIR /src/app
COPY package.json /src/app/
RUN npm install
COPY . /src/app
RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]