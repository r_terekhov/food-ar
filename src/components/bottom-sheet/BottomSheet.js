import React, { Component } from "react";
import SwipeableBottomSheet from "react-swipeable-bottom-sheet";
import OverflowScrolling from "react-overflow-scrolling";
import "./BottomSheet.css";

export class BottomSheet extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedDish: 0
    };

    this.changeModel = this.changeModel.bind(this);
  }

  componentDidMount() {
    var self = this;
    document.addEventListener("click", e => {
      if (e.target.id != "") {
        self.setState({ selectedDish: Number(e.target.id[4]) });
      }
    });
  }

  changeModel(id) {
    var marker = document.getElementsByTagName("a-marker")[0];
    switch (id % 5) {
      case 0:
        marker.innerHTML =
          "<a-entity animation='property: rotation; to: 0 360 0; loop: true; dur: 20000' gltf-model='url(https://foodarfiles.avocadoonline.company/files/food-models/meat/scene.gltf)'></a-entity>";
        break;
      case 1:
        //banana
        marker.innerHTML =
            "<a-entity scale='2 2 2' position='0 0 -3.3' gltf-model='url(https://foodarfiles.avocadoonline.company/files/food-models/banana/scene.gltf)'></a-entity>";
        break;
      case 2:
        marker.innerHTML =
            "<a-entity animation='property: rotation; to: 0 360 0; loop: true; dur: 20000' gltf-model='url(https://foodarfiles.avocadoonline.company/files/food-models/sushi_roll/scene.gltf)'></a-entity>";
        break;
      case 3:
        marker.innerHTML =
            "<a-entity animation='property: rotation; to: 0 360 0; loop: true; dur: 20000' scale='0.2 0.2 0.2' obj-model= 'obj: url(https://foodarfiles.avocadoonline.company/files/food-models/dog/french_bulldog.obj); mtl: url(https://food.demo.avocadoonline.company/files/food-models/dog/french_bulldog.mtl)'></a-entity>";
        break;
      case 4:
        marker.innerHTML =
            "<a-entity scale='0.03 0.03 0.03' animation='property: rotation; to: 0 360 0; loop: true; dur: 20000' gltf-model='url(https://foodarfiles.avocadoonline.company/files/food-models/baby_yoda/scene.gltf)'></a-entity>";
        break;
    }
  }

  render() {
    var dishes = [];
    for (let i = 0; i < 5; i++) {
      var className =
        this.state.selectedDish == i ? "dish selectedDish" : "dish";
      dishes.push(
        <div
          className={className}
          id={"dish" + i}
          onClick={() => this.changeModel(i)}
        ></div>
      );
    }

    switch (this.props.selectedCategory) {
      case "salads":
        var sc = "Салаты";
        break;
      case "appers":
        var sc = "Закуски";
        break;
      case "soups":
        var sc = "Супы";
        break;
      case "pizza":
        var sc = "Пиццы";
        break;
      case "pasts":
        var sc = "Пасты";
        break;
      case "hot":
        var sc = "Горячие блюда";
        break;
      case "desserts":
        var sc = "Десерты";
        break;
      case "childs":
        var sc = "Детское меню";
        break;
      case "drinks":
        var sc = "Напитки";
        break;
      case "alcohol":
        var sc = "Алкоголь";
        break;
      default:
        var sc = "Категория блюда";
        break;
    }

    return (
      <div>
        <div className="back-arrow-wrapper">
          <i
            className="fas fa-arrow-left back-arrow fa-lg"
            onClick={this.props.callback}
          ></i>
        </div>
        <SwipeableBottomSheet overflowHeight={210} overlay={true}>
          <div className="head">
            <h1 className="header">{sc}</h1>
            <div className="scrollWrapper">{dishes}</div>
            <h2 className="dishName">
              Стейк Мачете со средиземноморским соусом [
              {this.state.selectedDish}]
            </h2>
          </div>
          <div className="body">
            <div className="bodyWrapper">
              <h2 className="descriptionHeader">
                Сочная грудинка, приготовленная на гриле и аппетитная глазунья.
                Подается с гарниром на выбор, овощным салатом и соусом «Asia».
                <br />
                <br />
                Основное блюдо: 190г.
                <br />
                Гарнир: 140г.
                <br />
                Овощной салат: 60г.
                <br />
                Соус: 50г.
              </h2>
            </div>
          </div>
        </SwipeableBottomSheet>
      </div>
    );
  }
}
