import React, { Component } from "react";
import "./Menu.css";

export class Menu extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    document.addEventListener("click", e => {
      if (e.target.id != "") {
        this.props.callback(e.target.id);
      }
    });
  }

  render() {
    return (
      <div className="menu">
        <h1 className="menu-header">Меню</h1>
        <div className="menu-category-wrapper">
          <div className="menu-category" id="salads">
            <div className="menu-category-image salads-image" id="salads"></div>
            <h2 className="menu-category-header" id="salads">
              Салаты
            </h2>
          </div>
          <div className="menu-category" id="appers">
            <div className="menu-category-image appers-image" id="appers"></div>
            <h2 className="menu-category-header" id="appers">
              Закуски
            </h2>
          </div>
          <div className="menu-category" id="soups">
            <div className="menu-category-image soups-image" id="soups"></div>
            <h2 className="menu-category-header" id="soups">
              Супы
            </h2>
          </div>
          <div className="menu-category" id="pizza">
            <div className="menu-category-image pizza-image" id="pizza"></div>
            <h2 className="menu-category-header" id="pizza">
              Пиццы
            </h2>
          </div>
          <div className="menu-category" id="pasts">
            <div className="menu-category-image pasts-image" id="pasts"></div>
            <h2 className="menu-category-header" id="pasts">
              Пасты
            </h2>
          </div>
          <div className="menu-category" id="hot">
            <div className="menu-category-image hot-image" id="hot"></div>
            <h2 className="menu-category-header" id="hot">
              Горячие блюда
            </h2>
          </div>
          <div className="menu-category" id="desserts">
            <div
              className="menu-category-image desserts-image"
              id="desserts"
            ></div>
            <h2 className="menu-category-header" id="desserts">
              Десерты
            </h2>
          </div>
          <div className="menu-category" id="childs">
            <div className="menu-category-image childs-image" id="childs"></div>
            <h2 className="menu-category-header" id="childs">
              Детское меню
            </h2>
          </div>
          <div className="menu-category" id="drinks">
            <div className="menu-category-image drinks-image" id="drinks"></div>
            <h2 className="menu-category-header" id="drinks">
              Напитки
            </h2>
          </div>
          {/*<div className="menu-category" id="cocktails">*/}
          {/*  <div*/}
          {/*    className="menu-category-image cocktails-image"*/}
          {/*    id="cocktails"*/}
          {/*  ></div>*/}
          {/*  <h2 className="menu-category-header" id="cocktails">*/}
          {/*    Коктейль*/}
          {/*  </h2>*/}
          {/*</div>*/}
          <div className="menu-category" id="alcohol">
            <div
              className="menu-category-image alcohol-image"
              id="alcohol"
            ></div>
            <h2 className="menu-category-header" id="alcohol">
              Алкоголь
            </h2>
          </div>
        </div>
      </div>
    );
  }
}
