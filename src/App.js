import React, { Component } from "react";
import { BottomSheet } from "./components/bottom-sheet/BottomSheet";
import { Menu } from "./components/menu/Menu";

class App extends Component {
  constructor() {
    super();
    this.state = {
      selectedCategory: ""
    };
    this.selectCategory = this.selectCategory.bind(this);
    this.backToMenu = this.backToMenu.bind(this);
  }

  selectCategory(category) {
    console.log("callback!");
    this.setState({ selectedCategory: category });
  }

  backToMenu() {
    this.selectCategory("");
  }

  render() {
    var content =
      this.state.selectedCategory == "" ? (
        <Menu callback={this.selectCategory} />
      ) : (
        <BottomSheet callback={this.backToMenu} selectedCategory={this.state.selectedCategory}/>
      );
    return <div>{content}</div>;
  }
}

export default App;
