#!/bin/bash

DEFAULT_TAG=latest
read -p "Image TAG($DEFAULT_TAG): " TAG
TAG=${TAG:-$DEFAULT_TAG}

git pull
#npm install
docker build -t registry.gitlab.com/p-t/food-ar:$TAG .
docker push registry.gitlab.com/p-t/food-ar